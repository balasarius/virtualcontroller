Virtual Controller (ETS2 - Wheel Hack)
====================
This project is originally a workaround for the off center PC Steering Wheel on the Linux native version of Euro Truck Simulator 2. This will should work with any title with a centring issue on the steering wheel.

This project will also help older devices work correctly with the newer evdev drivers on linux.

This program will pass the target controller through to a new virtual event device which converts the wheel axis to a standard joystick axis.

once running start ETS2 and select the controller named "Virtual Controller" (or what ever name you have assign) and set it up just like any normal PC wheel.

this program will allow to use another event device or a js device.



System Requirements
---------------------
Linux

+ python-dev 
+ python-evdev >= 1.2.0



## Install

### Virtualenv and Git
Download latest build with git
````
git clone git clone git@bitbucket.org:balasarius/virtualcontroller.git
````
go into directory and setup virtual env including dependencies
````
cd virtualcontroller
virtualenv -p python2 venv
source venv/bin/activate
pip install -r requirements.txt
````

### Ubuntu

    sudo apt-get install python-dev python-evdev

### Other    
for further installation instruction for python-evdev
https://pythonhosted.org/evdev/install.html


Usage:
------
    $ python ./VirtualController.py --list
    
    Event Devices
    /dev/input/event2 || Driving Force GT
    
    
    js Devices
    /dev/input/js0

    
    $ sudo python VirtualController.py -d event2
    [sudo] password for balasarius: 
    
    Physical Controller:    Driving Force GT
    {('EV_MSC', 4L): [('MSC_SCAN', 4L)], ('EV_KEY', 1L): [(['BTN_JOYSTICK', 'BTN_TRIGGER'], 288L),
    ('BTN_THUMB', 289L), ('BTN_THUMB2', 290L), ('BTN_TOP', 291L), ('BTN_TOP2', 292L), ('BTN_PINKIE', 293L),
    ('BTN_BASE', 294L), ('BTN_BASE2', 295L), ('BTN_BASE3', 296L), ('BTN_BASE4', 297L), ('BTN_BASE5', 298L),
    ('BTN_BASE6', 299L), ('?', 300L), ('?', 301L), ('?', 302L), ('BTN_DEAD', 303L), (['BTN_TRIGGER_HAPPY',
    'BTN_TRIGGER_HAPPY1'], 704L), ('BTN_TRIGGER_HAPPY2', 705L), ('BTN_TRIGGER_HAPPY3', 706L),
    ('BTN_TRIGGER_HAPPY4', 707L), ('BTN_TRIGGER_HAPPY5', 708L)], ('EV_ABS', 3L): [(('ABS_X', 0L), AbsInfo
    (value=1156, min=0, max=16383, fuzz=0, flat=0, resolution=0)), (('ABS_Y', 1L), AbsInfo(value=255,
    min=0, max=255, fuzz=0, flat=0, resolution=0)), (('ABS_Z', 2L), AbsInfo(value=255, min=0, max=255,
    fuzz=0, flat=0, resolution=0)), (('ABS_HAT0X', 16L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0,
    resolution=0)), (('ABS_HAT0Y', 17L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))],
    ('EV_FF', 21L): [('FF_CONSTANT', 82L), ('FF_GAIN', 96L), ('FF_AUTOCENTER', 97L)], ('EV_SYN', 0L):
    [('SYN_REPORT', 0L), ('SYN_CONFIG', 1L), ('SYN_DROPPED', 3L), ('?', 4L), ('?', 21L)]}

    Driving Force GT
    
    now connected

    Virtual Controller:    Virtual Controller
    {('EV_KEY', 1L): [(['BTN_JOYSTICK', 'BTN_TRIGGER'], 288L), ('BTN_THUMB', 289L), ('BTN_THUMB2', 290L),
    ('BTN_TOP', 291L), ('BTN_TOP2', 292L), ('BTN_PINKIE', 293L), ('BTN_BASE', 294L), ('BTN_BASE2', 295L),
    ('BTN_BASE3', 296L), ('BTN_BASE4', 297L), ('BTN_BASE5', 298L), ('BTN_BASE6', 299L), ('?', 300L), ('?',
    301L), ('?', 302L), ('BTN_DEAD', 303L), (['BTN_TRIGGER_HAPPY', 'BTN_TRIGGER_HAPPY1'], 704L), 
    ('BTN_TRIGGER_HAPPY2', 705L), ('BTN_TRIGGER_HAPPY3', 706L), ('BTN_TRIGGER_HAPPY4', 707L),
    ('BTN_TRIGGER_HAPPY5', 708L)], ('EV_ABS', 3L): [(('ABS_X', 0L), AbsInfo(value=0, min=-8191, max=8191,
    fuzz=0, flat=0, resolution=0)), (('ABS_Y', 1L), AbsInfo(value=0, min=0, max=255, fuzz=0, flat=0,
    resolution=0)), (('ABS_Z', 2L), AbsInfo(value=0, min=0, max=255, fuzz=0, flat=0, resolution=0)),
    (('ABS_HAT0X', 16L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0)), (('ABS_HAT0Y',
     17L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))], ('EV_SYN', 0L): [('SYN_REPORT',
     0L), ('SYN_CONFIG', 1L), ('SYN_DROPPED', 3L)]}
     
     Virtual Controller

     now created
     
     
     Ctrl+C to quit.


