'''
Created on 19 Jul 2014

@author: Balasarius (Joseph Williams)
'''
import evdev, sys

class device:
    def __init__(self, dev):
        
        
        self.axis_modifier = []
        
        try:
            self.dev = evdev.InputDevice(dev)
            print("\n Physical Controller:    "+ str(self.dev.name) + "\n" + str(self.dev.capabilities(True, True)) + "\n")
            print(str(self.dev.name) + "\n\n now connected")
            self.dev.grab()
        except:
            print("Error: Device not Connected")
            sys.exit(1)
        
        data = self.dev.capabilities()
        phy_buttons = data[1L]
        phy_axis = data[3L]
        virt_buttons = []
        virt_axis = []
        
        for key in phy_buttons:
            virt_buttons.append(key)
        
        for key in phy_axis:
            if key[1][1] == 0 and key[1][2] > 255:
                minaxis = 0 - (key[1][2] / 2)
                maxaxis = key[1][2] / 2
                r = (int(key[0]), (0, minaxis, maxaxis, 0, 0, 0))
                self.axis_modifier.append((int(key[0]), maxaxis, 0, 0, 0))
            else:
                r = (int(key[0]), (0, key[1][1], key[1][2], key[1][3], key[1][4], 0, ))
            virt_axis.append(r)
        
        self.virtual_capabilities = {evdev.ecodes.EV_KEY: virt_buttons, evdev.ecodes.EV_ABS: virt_axis}
    def __del__(self):
        try:
            self.virtual_device.close()
            self.dev.ungrab()
        except:
            pass

        
    def create_virtual_controller(self, Name):
            
        try:
            print(self.virtual_capabilities)
            self.virtual_device = evdev.UInput(self.virtual_capabilities, name=Name, bustype=0x3)
            print("\n Virtual Controller:    " + str(self.virtual_device.name) + "\n" + str(self.virtual_device.capabilities(True, True)) + "\n\n")
            print(self.virtual_device.name+"\n\n"+
                  "now created\n")
        except:
            print("\n Fail to create Virtual Controller, please ensure you have valid permissions to '/dev/input/' or simply run as 'sudo'")
            sys.exit(1)

    
    def read_loop(self):
        for event in self.dev.read_loop():
            if event.type == evdev.ecodes.EV_ABS:
                for ax in self.axis_modifier:
                    if event.code == ax[0]:
                        out = event.value-ax[1]
                        return(evdev.ecodes.EV_ABS, event.code,out)                     
                    else:
                        return(evdev.ecodes.EV_ABS, event.code,event.value)
                     
            if event.type == evdev.ecodes.EV_KEY:
                return(evdev.ecodes.EV_KEY, event.code,event.value)
            
            
            
            
            
            