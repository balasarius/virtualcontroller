'''
Created on 19 Jul 2014

@author: Balasarius (Joseph Williams)
'''

import evdev, sys

class device:
    def __init__(self, dev):
        

        
        self.dev = dev
        self.pipe = open(self.dev, 'r')
        
        #dictionaries for a generic virtual device and translation
        self.axis_dict      = {0:['ABS_RX',3],
                               1:['ABS_Y',1],
                               2:['ABS_Z',2],
                               3:['ABS_X',0],
                               4:['ABS_RY',4],
                               5:['ABS_RZ',5]}
        self.button_dict    = {0:(['BTN_JOYSTICK', 'BTN_TRIGGER'], 288), 
                               1:('BTN_TRIGGER_HAPPY2',705),
                               2:('BTN_TRIGGER_HAPPY3',706),
                               3:('BTN_TRIGGER_HAPPY4',707),
                               4:('BTN_TRIGGER_HAPPY5',708),
                               5:('BTN_TRIGGER_HAPPY6',709),
                               6:('BTN_TRIGGER_HAPPY7',710), 
                               7:('BTN_TRIGGER_HAPPY8',711), 
                               8:('BTN_TRIGGER_HAPPY9',712),
                               9:('BTN_TRIGGER_HAPPY10',713),
                               10:('BTN_TRIGGER_HAPPY11',714),
                               11:('BTN_TRIGGER_HAPPY12',715), 
                               12:('BTN_TRIGGER_HAPPY13',716),
                               13:('BTN_TRIGGER_HAPPY14',717),
                               14:('BTN_TRIGGER_HAPPY15',718),
                               15:('BTN_TRIGGER_HAPPY16',719),
                               16:('BTN_TRIGGER_HAPPY17',720),
                               17:('BTN_TRIGGER_HAPPY18',721),
                               18:('BTN_TRIGGER_HAPPY19',722),
                               19:('BTN_TRIGGER_HAPPY20',723),
                               20:('BTN_TRIGGER_HAPPY21',724),
                               21:('BTN_TRIGGER_HAPPY22',725),
                               22:('BTN_TRIGGER_HAPPY23',726),
                               23:('BTN_TRIGGER_HAPPY24',727),
                               24:('BTN_TRIGGER_HAPPY25',728),
                               25:('BTN_TRIGGER_HAPPY26',729),
                               26:('BTN_TRIGGER_HAPPY27',730),
                               27:('BTN_TRIGGER_HAPPY28',731), 
                               28:('BTN_TRIGGER_HAPPY29',732),
                               29:('BTN_TRIGGER_HAPPY30',733),
                               30:('BTN_TRIGGER_HAPPY31',734)}
        
        self.virtual_capabilities = {1: [(self.button_dict[0][1]),
                                            (self.button_dict[1][1]),
                                            (self.button_dict[2][1]),
                                            (self.button_dict[3][1]),
                                            (self.button_dict[4][1]),
                                            (self.button_dict[5][1]),
                                            (self.button_dict[6][1]),
                                            (self.button_dict[7][1]),
                                            (self.button_dict[8][1]),
                                            (self.button_dict[9][1]),
                                            (self.button_dict[10][1]),
                                            (self.button_dict[11][1]),
                                            (self.button_dict[12][1]),
                                            (self.button_dict[13][1]),
                                            (self.button_dict[14][1]),
                                            (self.button_dict[15][1]),
                                            (self.button_dict[16][1]),
                                            (self.button_dict[17][1]),
                                            (self.button_dict[18][1]),
                                            (self.button_dict[19][1]),
                                            (self.button_dict[20][1]),
                                            (self.button_dict[21][1]),
                                            (self.button_dict[22][1]),
                                            (self.button_dict[23][1]),
                                            (self.button_dict[24][1]),
                                            (self.button_dict[25][1]),
                                            (self.button_dict[26][1]),
                                            (self.button_dict[27][1]),
                                            (self.button_dict[28][1]),
                                            (self.button_dict[29][1]),
                                            (self.button_dict[30][1])], 
                                     3: [((self.axis_dict[0][1]),(0, -32640, 32640, 0, 0, 0)),
                                           ((self.axis_dict[1][1]),(0, -32640, 32640, 0, 0, 0)),
                                           ((self.axis_dict[2][1]),(0, -32640, 32640, 0, 0, 0)),
                                           ((self.axis_dict[3][1]),(0, -32640, 32640, 0, 0, 0)),
                                           ((self.axis_dict[4][1]),(0, -32640, 32640, 0, 0, 0)),
                                           ((self.axis_dict[5][1]),(0, -32640, 32640, 0, 0, 0))]}
    
    def __del__(self):
        try:
            self.pipe.close()
            self.virtual_device.close()
        except:
            pass
    
    def create_virtual_controller(self, Name):
        
        try:
            self.virtual_device = evdev.UInput(self.virtual_capabilities, name=Name, bustype=0x3)
            print("\n Virtual Controller:    "+ str(self.virtual_device.name) + "\n" + str(self.virtual_device.capabilities(True, True)) + "\n\n")
            print(self.virtual_device.name+"\n\n"+
                  "now created\n")
        except:
            print("\n Fail to create Virtual Controller, please ensure you have valid permissions to '/dev/input/' or simply run as 'sudo'")
            
            sys.exit(1)
               
    def read_loop(self):
        
        
        msg = []
        while True:
            for char in self.pipe.read(1):
                msg += [ord(char)]
                
                if len(msg) == 8:
                    
                    # Button Event
                    if msg[6] == 1:
                                                
                        self.type = 1
                        self.ecode = self.button_dict[msg[7]][1]
                        self.value = msg[4]
                        return(self.type, self.ecode, self.value)
                        
                    # Axis Event
                    elif msg[6] == 2:
                        axis_range = ((msg[5]*255)+msg[4])
                        if axis_range > 32640:
                            axis = axis_range-32640
                        else:
                            axis = axis_range+32640
                        realaxis = (axis - 32640)
                        
                        self.type = 3
                        self.ecode = self.axis_dict[msg[7]][1]
                        self.value = realaxis
                        return(self.type, self.ecode, self.value)
                    
                    msg = []
            
    

    

