"""
Created on 19 Jul 2014

@author: Balasarius (Joseph Williams)
"""

from dev import *
import optparse, sys, signal, evdev, os, glob

def sigint_handler(signal, frame):
    print("Quitting.")

    sys.exit(0)
    
if __name__=="__main__":  
    
    parser = optparse.OptionParser("usage: \t python %prog -l, --list  \n"+
                                          "\t python %prog -d, --device [event* or js*] \n"+
                                          "\t python %prog -v, --verbose"+
                                          "\t python %prog -n, --name [Virtual Controller name] \n") 
    parser.add_option("-l", "--list",
                      action="store_true", dest="list", default=False,
                      help="List Devices")
    parser.add_option("-v", "--Verbose",
                      action="store_true", dest="verbose", default=False,
                      help="Debuging")
    parser.add_option("-d", "--device", dest="device", default="",
                      type="string", help="Use Device")
    parser.add_option("-n", "--name", dest="name", default="Virtual Controller",
                      type="string", help="Virtual Controller Name")
    
    (options, args) = parser.parse_args()
    
    # Apply Verbose Mode if requested
    if options.verbose == True:
        debug = 1
    else:
        debug = 0
        
    # list Devices
    if options.list == True:
        devices = map(evdev.InputDevice, evdev.list_devices())
        print("\nEvent Devices")
        for dev in devices:
            print(str(dev.fn) + " || "  + str(dev.name) + "\n" )
            if debug == 1:
                print("\n\n" + str(dev.capabilities(True, True)))
            print("\njs Devices")
            os.chdir("/dev/input")
            for js in glob.glob("js*"):
                print("/dev/input/"+js+"\n")
        sys.exit(0)
    
    # Attach and use device        
    elif options.device != "":
        
        # access physical Device
        device_path = options.device
        if device_path[:1] == "j":
            device = js.device("/dev/input/"+device_path)
        elif device_path[:1] == "e":
            device = ev.device("/dev/input/"+device_path)
            
        # create virtual device
        device.create_virtual_controller(options.name)
        
        # Quit Signal handler
        signal.signal(signal.SIGINT, sigint_handler)
        print("Ctrl+C to quit.")
        
        # Main Loop
        while True:
            out = device.read_loop()
            if debug == 1:
                print("type: "+ str(out[0])+" id: "+str(out[1])+" value: "+str(out[2]))
            device.virtual_device.write(out[0],out[1],out[2])
            device.virtual_device.syn()

